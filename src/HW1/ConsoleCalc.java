package HW1;

import java.util.Scanner;

public class ConsoleCalc {
    public static void main(String[] args) {
        double a, b;
        String inputString;
        char operation = '0';
        Scanner sc = new Scanner(System.in);
        //ввод первого числа
        System.out.println("Введите первое число:");
        while (!sc.hasNextDouble()) {
            sc.next();
            System.out.println("Ошибка!!! Введено не число!!! Повторите ввод числа");
        }
        a = sc.nextDouble();

        //ввод операции
        System.out.println("Введите требуемую операцию:");
        while (operation == '0') {
            inputString = sc.next();
            switch (inputString.charAt(0)) {
                case '-':
                    operation = '-';
                    break;
                case '+':
                    operation = '+';
                    break;
                case '*':
                    operation = '*';
                    break;
                case '/':
                    operation = '/';
                    break;
                default:
                    System.out.println("Введена некорректная операция. Повторите ввод операции");
            }
        }

        //ввод второго числа
        System.out.println("Введите второе число:");
        while (!sc.hasNextDouble()) {
            sc.next();
            System.out.println("Ошибка!!! Введено не число!!! Повторите ввод числа");
        }
        b = sc.nextDouble();

        switch (operation) {
            case '-':
                System.out.printf("%f - %f = %f", a, b, a-b);
                break;
            case '+':
                System.out.printf("%f + %f = %f",a,b,a+b);
                break;
            case '*':
                System.out.printf("%f * %f = %f",a,b,a*b);
                break;
            case '/':
                if ( b == 0) {
                    System.out.println("Делить на ноль нельзя!!!!");
                }
                else System.out.printf("%f / %f = %f",a,b,a/b);
                break;
            default:
                System.out.println("Произошла ошибка");
                break;
        }

    }
}
